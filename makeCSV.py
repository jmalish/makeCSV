# imports
import csv
import math
import os.path

# # variables
csvData = []  # stores csv data

# user inputs
saveLoc = str(raw_input("Save Location: ") or "")
nUp = int(raw_input("How many up (1): ") or 1)
needed = int(raw_input("Quantity (1000): ") or 1000)
startNum = int(raw_input("Starting number (1): ") or 1)
leadingZeros = int(raw_input("How many leading zeros (0): ") or 0) + 1
receiptStyle = bool(raw_input("Receipt style? [y/(n)]: ")
                    .lower() == "y" or False)


rowsNeeded = int(math.ceil(needed/nUp))  # calculate how many rows we need

# build headers
headers = []
for i in range(0, nUp):
    colName = 'col' + str(i+1)
    headers.append(colName)

csvData.append(headers)  # add headers to csv data

# build numbers array
if (not receiptStyle):
    for i in range(0, rowsNeeded):
        row = []
        for j in range(0, nUp):
            row.append(str((i + (rowsNeeded * j) + startNum))
                       .zfill(leadingZeros))  # add the leading zeros
        csvData.append(row)
else:  # receipt style numbering
    for i in range(0, rowsNeeded):
        row = []
        for j in range(0, nUp):
            row.append(str(j + startNum + (i * nUp))
                       .zfill(leadingZeros))  # leading zeros
        csvData.append(row)


# build file name, split up to avoid the "lines too long" thing
fileName = str(nUp) + "up " + str(startNum) + "-"
fileName = fileName + str(csvData[len(csvData) - 1][nUp - 1])

if (receiptStyle):
    fileName = fileName + " receiptStyle"

fileName = fileName + ".csv"

savePath = os.path.join(saveLoc, fileName)

# create and write to file
with open(savePath, 'wb') as csvFile:
    writer = csv.writer(csvFile)
    writer.writerows(csvData)

csvFile.close()
print savePath + " created!"
